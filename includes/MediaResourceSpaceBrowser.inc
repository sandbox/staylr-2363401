<?php

/**
 * @file media_resourcespace/includes/MediaResourceSpaceBrowser.inc
 *
 * Definition of MediaResourceSpaceBrowser.
 */

/**
 * Media browser plugin for displaying a specific view and display.
 */
class MediaResourceSpaceBrowser extends MediaBrowserPlugin {
  /**
   * Implements MediaBrowserPluginInterface::access().
   */
  public function access($account = NULL) {
    return file_entity_access('create', NULL, $account);
  }

  /**
   * Implements MediaBrowserPlugin::view().
   */
  public function view() {
    // Check if the user is able to add remote media.
    if (user_access('add media from remote sources')) {
      $build['form'] = drupal_get_form('media_resourcespace_add', $this->params['types'], $this->params['multiselect']);
      return $build;
    }
  }
}
