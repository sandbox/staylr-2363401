<?php

require_once dirname(__FILE__) . '/media_resourcespace.utilities.inc';

/**
 * @file
 * Extends the MediaInternetBaseHandler class to handle ResourceSpace images.
 */

/**
 * Implementation of MediaInternetBaseHandler.
 *
 * @see hook_media_internet_providers().
 */
class MediaInternetResourceSpaceHandler extends MediaInternetBaseHandler {
  /**
  ** Cache to avoid repeated round trips to ResourceSpace.
  */
  protected static $processedEmbedCodes = array();

  public function parse($embedCode) {
    $result = self::parse_embed_code($embedCode);
    if (isset($result)) {
      return $result['uri'];
    }

    return NULL;
  }

  protected function parse_embed_code($embedCode) {
    if (isset(self::$processedEmbedCodes[$embedCode])) {
      $result = self::$processedEmbedCodes[$embedCode][__METHOD__];
    }
    else {
      $result = media_resourcespace_media_parse_with_metadata($embedCode);
      self::$processedEmbedCodes[$embedCode][__METHOD__] = $result;
    }

    return $result;
  }

  public function claim($embedCode) {
    return ($this->parse($embedCode) ? TRUE : FALSE);
  }

  /**
   * Returns the current resourcespace:// URI.
   */
  public function getResourceSpaceUri() {
    return $this->parse($this->embedCode);
  }

  /**
   * Returns the current local URI.
   */
  public function getLocalUri() {
    return _media_resourcespace_to_local_uri($this->getResourceSpaceUri());
  }

  /**
   * Returns the external URL of the original ResourceSpace image.
   */
  public function getExternalResourceSpaceUrl() {
    $wrapper = file_stream_wrapper_get_instance_by_uri(
        $this->getResourceSpaceUri());
    return $wrapper->getExternalUrl();
  }

  /**
   * Overrides MediaInternetResourceSpaceHandler::getFileObject().
   */
  public function getFileObject() {
    $info = $this->parse_embed_code($this->embedCode);

    // If the local file already exists, just return that.
    $local_uri = _media_resourcespace_to_local_uri($info['uri']);
    $file = file_uri_to_object($local_uri, TRUE);
    $file = _media_resourcespace_set_file_data($file, $info);

    return $file;
  }

  public function validate() {
    $file = $this->getFileObject();
    $validators = array();
    return _media_resourcespace_validate_file($file, $validators);
  }

  public function preSave(&$file) {
    $remote_url = $this->getExternalResourceSpaceUrl();
    return _media_resourcespace_download_resource($file, $remote_url);
  }

  // Set the title and alt text fields.
  public function postSave(&$file) {
    $saved_file = file_load($file->fid);
    _media_resourcespace_save_title_and_alt($saved_file,
                            $file->title, $file->alt);
  }
}
