<?php

/**
 * @file
 * Admin page callbacks for the Media: ResourceSpace module.
 */

/**
 * Form builder; The Media: ResourceSpace configuration form.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function media_resourcespace_settings_form() {
  $rs_server_url = variable_get('media_resourcespace_server_url', '');

  $form['#prefix'] = '<div id="media_resourcespace_admin_div">';
  $form['#suffix'] = '</div>';

  $form['media_resourcespace_server_url'] = array(
      '#type' => 'textfield',
      '#title' => t('ResourceSpace server url'),
      '#default_value' => $rs_server_url,
      '#rules' => array('url[absolute]'),
      '#ajax' => array(
        'callback' => 'media_resourcespace_api_url_callback',
        'wrapper' => 'media_resourcespace_admin_div',
        'method' => 'replace'
        )
      );

  $form['media_resourcespace_api_link'] = array(
      '#type' => 'link',
      '#title' => 'Visit ResourceSpace to get an authentication key',
      '#prefix' => '<div id="media_resourcespace_api_url_div">',
      '#suffix' => '</div>',
      '#href' => '' // Filled in below.
      );

  media_resourcespace_set_api_url($form, $rs_server_url);

  $form['media_resourcespace_api_key'] = array(
      '#type' => 'textfield',
      '#title' => t('ResourceSpace authentication key'),
      '#default_value' => variable_get('media_resourcespace_api_key', ''),
      );

  return system_settings_form($form);
}

function media_resourcespace_api_url_callback(&$form, &$form_state) {
  if (isset($form_state['values']['media_resourcespace_server_url'])) {
    $rs_server_url = $form_state['values']['media_resourcespace_server_url'];

    if (substr($rs_server_url, 0, 4) != 'http') {
      $rs_server_url = "https://$rs_server_url";
      $form['media_resourcespace_server_url']['#value'] = $rs_server_url;
    }

    media_resourcespace_set_api_url($form, $rs_server_url);
  }

  return $form;
}

function media_resourcespace_set_api_url(&$form, $rs_server_url) {
  $link_address = media_resourcespace_compute_api_url($rs_server_url);
  $form['media_resourcespace_api_link']['#href'] = $link_address;
}

function media_resourcespace_compute_api_url($rs_server_url) {
  $api_url = $rs_server_url;

  if (empty($api_url)) {
    $api_url = variable_get('media_resourcespace_server_url', '');
  }

  if (empty($api_url)) {
    $api_url = 'http://<rs_server>';
  }

  return rtrim($api_url, '/') . '/plugins/api_core/index.php';
}
