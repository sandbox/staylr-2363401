<?php

require_once dirname(__FILE__) . '/media_resourcespace.utilities.inc';

define('RESOURCESPACE_THUMB_SIZE', 'thm');
define('RESOURCESPACE_ORIGINAL_SIZE', 'orig');
define('RESOURCESPACE_COLLECTION_THUMB_SIZE', 'col');
define('RESOURCESPACE_RESULTS_PER_PAGE', '10');

/*
** @file 
** Creates a ResourceSpace search tab for Media Browser.
*/
function _media_resourcespace_browser_add($form, &$form_state = array()) {
  $term = _media_resourcespace_get_input_data('resourcespace_search',
                          $form_state, '');
  $default_resource_types = array_keys(_media_resourcespace_resource_types());
  $resource_types = _media_resourcespace_get_input_data('resourcespace_types',
                          $form_state, $default_resource_types);
  $date = _media_resourcespace_get_input_data('resourcespace_date',
                          $form_state, NULL);

  _media_resourcespace_browser_search_fields($term, $resource_types, $date,
                          $form, $form_state);

  // This is our ghetto pager.
  $page = isset($_GET['page-yt']) ? $_GET['page-yt'] : 0;
  if (isset($form_state['input']['resourcespace_search'])) {
    // Reset the pager when we press apply.
    $page = 0;
  }

  if (!empty($term) || !empty($date['year']) || !empty($date['month'])) {
    $results_per_page = RESOURCESPACE_RESULTS_PER_PAGE;
    $search = _media_resourcespace_search_by_term($term,
                  $resource_types, $date, $results_per_page, $page + 1);
  }

  $form['resources']['#prefix'] = '<div id="container"><div id="scrollbox"><table id="media-rs-library-list" class="media-rs-result-table">';
  $form['resources']['#suffix'] = '</table><div id="status"></div></div></div>';

  $empty = FALSE;
  $files = array();
  if (!isset($search) || !is_object($search)
      || count($search->resources) == 0) {
    $empty = TRUE;
  }
  else {
    foreach ($search->resources as $resource) {
      $resource_markup = _media_resourcespace_browser_resource_markup(
                            $resource, $files);

      if (isset($resource_markup)) {
        $form['resources'][$resource_markup['uri']] = array(
          '#markup' => $resource_markup['markup'],
          '#prefix' => '<tr class="media-rs-item">',
          '#suffix' => '</tr>'
        );
      }
    }
  }

  if (!count($files)) {
    $empty= TRUE;
  }
  if ($empty) {
    $form['empty'] = array(
      '#markup' => '<div class="empty-message">' . t('No resources match your search criteria. Please try again.') . '</div>',
    );
  }

  $query = $_GET;
  if ($term !== '') {
    $query['resourcespace_search'] = $term;
  }

  $dest = $query['q'];
  unset($query['q']);
  $prev = $next = '';
  if ($page) {
    $query['page-yt'] = $page - 1;
    $prev = l(t('previous'), $dest,
          array('query' => $query,
                'fragment' => 'media-tab-resourcespace'));
  }
  $query['page-yt'] = $page + 1;
  if (!$empty) {
    $next = l(t('next'), $dest,
          array('query' => $query,
                'fragment' => 'media-tab-resourcespace'));
  }

  $form['pager']= array(
    '#markup' => $prev . ' ' . $next,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  // Add javascript and CSS.
  $path = drupal_get_path('module', 'media_resourcespace');
  $form['#attached']['css'][] = $path . '/css/media_resourcespace.css';
  $form['#attached']['js'][] = $path . '/js/media_resourcespace.browser.js';

  // Add the files to JS so that they are accessible inside the browser
  drupal_add_js(array('media' => array('files' => $files)), 'setting');

  return $form;
}

/**
  * Set up the search fields for a ResourceSpace search.
  */
function _media_resourcespace_browser_search_fields($term,
                  $resource_types, $date, &$form, &$form_state) {
  $form['resourcespace_search'] = array(
    '#type' => 'textfield',
    '#title' => t('Search'),
    '#description' =>
            t('Search using descriptions, keywords and resource numbers.'),
    '#default_value' => $term
  );

  $form['resourcespace_types'] = array(
    '#title' => t('Resource Types'),
    '#type' => 'checkboxes',
    '#options' => _media_resourcespace_resource_types(),
    '#default_value' => $resource_types);

  $form['resourcespace_date'] = array(
    '#type' => 'date_select',
    '#title' => t('By Date'),
    '#date_format' => 'm-Y',
    '#default_value' => $date,
    '#date_year_range' => '1980:0',
    '#required' => FALSE,
    '#date_label_position' => 'within');

  $form['resourcespace_apply'] = array(
    '#type' => 'button',
    '#value' => t('Apply'));

  $form['submitted-resource'] = array(
    '#type' => 'hidden',
    '#default_value' => FALSE,
  );

  $form['submitted-resource-data'] = array(
    '#type' => 'hidden',
    '#default_value' => FALSE,
  );
}

/**
** Our search term can come from the form, or from the pager.
**/
function _media_resourcespace_get_input_data($field_name,
                                  $form_state, $default) {
  $form_state_field = @$form_state['input'][$field_name];

  if (isset($form_state_field)) {
    return $form_state_field;
  }
  else {
    $get_field = @$_GET[$field_name];

    if (isset($get_field)) {
      return $get_field;
    }
  }

  return $default;
}

/**
  * Create a HTML table entry for a single resource and all available sizes.
  */
function _media_resourcespace_browser_resource_markup(&$resource, &$files) {
  $remote_view_uri = check_url(_media_resourcespace_remote_view_url(
                            $resource->ref));

  $orig_extension = '';
  $thumb_icon_path = '';

  $size_list_markup=
      '<ul class="media-list-thumbnails media-rs-size-list">';

  $file_info = NULL;
  $orig_file = NULL;
  $thunb_size = NULL;

  foreach ($resource->sizes as $resource_size) {
    $file_info = _media_resourcespace_build_search_result_info($resource,
                          $resource_size);

    // Skip smaller thumbnails for used for displaying
    // collections in ResourceSpace.
    if ($resource_size->id == RESOURCESPACE_COLLECTION_THUMB_SIZE) {
      continue;
    }

    if ($resource_size->id == RESOURCESPACE_THUMB_SIZE) {
      $thumb_size = $file_info;
    }

    $size_file = _media_resourcespace_browser_create_temp_file($file_info);

    if ($resource_size->id == RESOURCESPACE_ORIGINAL_SIZE) {
      $orig_file = $size_file;
    }

    // check_plain not check_url because this is a Drupal Media URL,
    // with scheme resourcespace://, which would be stripped out by check_url.
    $size_uri = check_plain($size_file->uri);

    $size_description = _media_resourcespace_size_description($file_info);

    $size_div = "<div class=\"media-item media-rs-media-item\">$size_description</div>";

    // Data to store on the form to be able to find the resource on submit.
    $form_info = array('ref' => $file_info['ref'],
                          'ext' => $file_info['ext'],
                          'size' => $file_info['size']);

    $size_link = l($size_div, 'media/browser', array(
      'html' => TRUE,
      'attributes' => array(
        'data-uri' => $size_uri,
        'data-resource-data' => drupal_json_encode($form_info)
      ),
      'query' => array('render' => 'media-popup', 'uri' => $size_uri)
    ));

    $size_list_markup .=
          '<li class="media-rs-resource-size">'
          . $size_link . '</li>';

    $files[$size_file->uri] = $size_file;
  }

  $size_list_markup .= '</ul>';

  $title=@$file_info['title'];
  $alt=@$file_info['alt'];

  if (isset($thumb_size)) {
    $thumb_uri = _media_resourcespace_get_thumbnail_data_uri($thumb_size['ext'],
                        _media_resourcespace_remote_url(
                            $resource->ref, $thumb_size['ext'],
                            RESOURCESPACE_THUMB_SIZE, TRUE));
    $thumb_class = "media-rs-thumbnail";
  }
  else {
    $thumb_uri = _media_resourcespace_get_generic_thumbnail_url($resource);
    $thumb_class = "media-rs-generic-thumbnail";
  }

  $thumb_link =
      "<div class=\"media-rs-thumbnail-div\"><span></span><a href=\"$remote_view_uri\" target=\"_blank\"><img class=\"$thumb_class\" title=\"$title\" alt=\"$alt\" src=\"$thumb_uri\" /></a></div>";

  $file_info_table = _media_resourcespace_create_file_info_table($file_info);
  $markup = "<td class=\"media-rs-result-table-thumb\">$thumb_link</td><td class=\"media-rs-result-table-detail\">$file_info_table</td><td class=\"media-rs-result-table-size\">$size_list_markup</td>";

  $orig_size_uri = _media_resourcespace_construct_url($resource,
                        $resource->file_extension,
                        MEDIA_RESOURCESPACE_SIZE_ORIGINAL);

  return array('uri' => $orig_size_uri, 'markup' => $markup);
}

/**
*/
function _media_resourcespace_get_generic_thumbnail_url($resource) {
  // Most of the functions to work out which thumbnail to use
  // rely on having the file locally, so just work it out ourselves.
  $resource_type = $resource->resource_type;
  if ($resource_type == 1) {
    $image = 'image-x-generic.png';
  }
  elseif ($resource_type == 2) {
    $image = 'application-octet-stream.png';
  }
  elseif ($resource_type == 3) {
    $image = 'video-x-generic.png';
  }
  elseif ($resource_type == 4) {
    $image = 'audio-x-generic.png';
  }
  else {
    $image = 'application-octet-stream.png';
  }

  $filepath = drupal_get_path('module', 'media')
              . '/images/icons/default/' . $image;
  return url($filepath, array('aboslute' => TRUE));
}

/**
** Build a table containing information for the current resource.
**/
function _media_resourcespace_create_file_info_table($file_info) {
  $title=@$file_info['title'];
  $alt=@$file_info['alt'];
  $orig_filename=@$file_info['orig_filename'];
  $creation_date=@$file_info['creation_date'];

  $file_info_table =
      '<dl class="media-rs-file-detail-table">';

  $file_info_table .=
        _media_resourcespace_create_file_info_row('ID:', $file_info['ref']);

  if (!empty($title)) {
    $file_info_table .=
        _media_resourcespace_create_file_info_row('Title:', $title);
  }

  if (!empty($alt) && $alt != $title) {
    $file_info_table .=
        _media_resourcespace_create_file_info_row('Alt Text:', $alt);
  }

  if (!empty($orig_filename)) {
    $file_info_table .=
        _media_resourcespace_create_file_info_row('File:', $orig_filename);
  }

  if (isset($creation_date)) {
    $date_obj = new DateObject($creation_date);
    $formatted_date = date_format_date($date_obj, 'custom', 'd F Y');
    $file_info_table .=
        _media_resourcespace_create_file_info_row('Created:', $formatted_date);
  }

  $file_info_table .= "</dl>";
  return $file_info_table;
}

function _media_resourcespace_create_file_info_row($heading, $value) {
  return "<dt>$heading</dt><dd>$value</dd>";
}

function _media_resourcespace_get_thumbnail_data_uri($thumb_ext, $uri) {
  $request = drupal_http_request($uri);
  if (!isset($request->error)) {
    $mapping = file_mimetype_mapping();

    if (isset($mapping['extensions'][$thumb_ext])) {
      $mime_type = $mapping['mimetypes'][$mapping['extensions'][$thumb_ext]];
    }
    else {
      throw new Exception("Unknown thumbnail extension $thumb_ext");
    }

    $data = base64_encode($request->data);
    return 'data:' . $mime_type . ';base64,' . $data;
  }
  else {
    form_set_error('url',
        t('Error loading thumbnail (%code): (%error).',
            array('%code' => $request->code, '%error' => $request->error)));
    return;
  }
}

/**
 * Create a temporary file for a resource in the browser resource list.
 */
function _media_resourcespace_browser_create_temp_file($file_info) {
  // Create a temporary file object for our retrieved resource.
  $file = file_uri_to_object($file_info['uri']);
  if (!isset($file->fid)) {
    $file->fid = 0;
  }

  return $file;
}

/**
 * Return a description for a ResourceSpace resource size.
 */
function _media_resourcespace_size_description($resource_size) {
  $size_description = '<span class="media-rs-size-name">'
        . $resource_size['size_name'] . '</span>';

  $size_description .=
        ' <span class="media-rs-size-detail">'
        . '(' . $resource_size['ext'] . ', ';

  if (!empty($resource_size['width']) && !empty($resource_size['height'])) {
    $size_description .=
        $resource_size['width'] . 'x' . $resource_size['height'] . 'px, ';
  }

  $size_description .= $resource_size['filesize'] . ')</span>';
  return $size_description;
}

/**
 * Basic file validation - size and resolution.
 */
function _media_resourcespace_browser_add_validate($form, &$form_state) {

  if ($form_state['values']['op'] == t('Apply')) {
    return;
  }

  $uri = $form_state['values']['submitted-resource'];

  if (empty($uri)) {
    form_set_error('url', t('Please select a resource.'));
    return;
  }

  $data = _media_resourcespace_browser_get_submitted_file_data($form_state);
  $file_uri = _media_resourcespace_to_local_uri($uri);
  try {
    $file = file_uri_to_object($file_uri, TRUE);
    $file = _media_resourcespace_set_file_data($file, $data);
  } catch (Exception $e) {
    form_set_error('url', $e->getMessage());
    return;
  }

  $validators = isset($form['#validators']) ? $form['#validators'] : array();
  return _media_resourcespace_validate_file($file, $validators);
}

/**
 * Save the file and forward to the edit page.
 */
function _media_resourcespace_browser_add_submit($form, &$form_state) {
  $uri = $form_state['values']['submitted-resource'];

  if (empty($uri)) {
    form_set_error('url', t('Please select a resource.'));
    return;
  }

  $data = _media_resourcespace_browser_get_submitted_file_data($form_state);

  $file_uri = _media_resourcespace_to_local_uri($uri);
  $remote_uri = _media_resourcespace_remote_url($data['ref'],
                          $data['ext'], $data['size']);

  try {
    // Save the remote file
    $file = file_uri_to_object($file_uri, TRUE);
    $file = _media_resourcespace_set_file_data($file, $data);
    _media_resourcespace_download_resource($file, $remote_uri);
    file_save($file);

    // The alt and title fields aren't created until the file is saved,
    // so reload and resave/ with the fields.
    $file = file_load($file->fid);
    $file = _media_resourcespace_set_file_data($file, $data);
    _media_resourcespace_save_title_and_alt($file,
                $file->title, $file->alt);
  }
  catch (Exception $e) {
    form_set_error('url', $e->getMessage());
    return;
  }

  if (!$file->fid) {
    form_set_error('url', t('The file %file could not be saved. An unknown error has occurred.', array('%file' => $file_uri)));
    return;
  }
  else {
    $form_state['file'] = $file;
    $form_state['storage']['upload'] = $file->fid;
  }

  // Redirect to the file edit page after submission.
  if (file_entity_access('update', $file)) {
    $destination = array('destination' => 'admin/content/file');
    if (isset($_GET['destination'])) {
      $destination = drupal_get_destination();
      unset($_GET['destination']);
    }
    $form_state['redirect'] = array('file/' . $file->fid . '/edit',
                                    array('query' => $destination));
  }
  else {
    $form_state['redirect'] = 'admin/content/file';
  }
}

/**
** Search ResourceSpace to get the data for the file that was submitted.
**/
function _media_resourcespace_browser_get_submitted_file_data($form_state) {
  $data_json = $form_state['values']['submitted-resource-data'];
  $search_data = drupal_json_decode($data_json);
  $ref = $search_data['ref'];
  $data = _media_resourcespace_search_by_ref_size($ref,
                  $search_data['ext'], $search_data['size']);
  if (!isset($data)) {
    throw new Exception("Could not find resource $ref");
  }

  return $data;
}
