<?php

require_once dirname(__FILE__) . '/media_resourcespace.utilities.inc';

/**
 *  @file
 *  Create a ResourceSpace Stream Wrapper class for the Media/Resource module.
 */

/**
 *  Create an instance like this:
 *  $resourcespace = new MediaResourceSpaceStreamWrapper('resourcespace://ref/[asset-id]');
 */
class MediaResourceSpaceStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url;
  protected $parameters = array('server' => NULL, 'ref' => NULL,
      'ext' => NULL, 'file' => NULL, 'size' => NULL);

  function __contruct() {
    parent::__construct();
    $base_url = _media_resourcespace_get_server_url();
  }

  function interpolateUrl() {
    if (isset($this->parameters['ref'])) {
      return _media_resourcespace_remote_url(
                check_plain($this->parameters['ref']),
                check_plain($this->parameters['ext']),
                check_plain($this->parameters['size']));
    }
  }

  function getTarget($f) {
    return FALSE;
  }

  /**
   * Get the url of the original image.
   */
  function getExternalUrl() {
    return $this->interpolateUrl();
  }

  public static function getMimeType($uri, $mapping = NULL) {
    return DrupalLocalStreamWrapper::getMimeType($uri, $mapping);
  }

  /**
   * Returns the external URL of the original ResourceSpace image.
   */
  public function getExternalResourceSpaceUrl() {
    $wrapper = file_stream_wrapper_get_instance_by_uri(
        $this->getResourceSpaceUri());
    return $wrapper->getExternalUrl();
  }
}
