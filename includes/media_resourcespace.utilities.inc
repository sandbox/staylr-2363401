<?php

/**
 *  @file
 *  Utility functions for Media: ResourceSpace.
 */

// Defaulting of sizes: if size is unset, use screen.
// If set to '', use the original.
define('MEDIA_RESOURCESPACE_SIZE_SCREEN', 'scr');

// This isn't actually a ResourceSpace size - 'size=' gets the
// original size.
define('MEDIA_RESOURCESPACE_SIZE_ORIGINAL', 'orig');

define('MEDIA_RESOURCESPACE_ALT_TEXT_FIELD', 'field_file_image_alt_text');
define('MEDIA_RESOURCESPACE_TITLE_FIELD', 'field_file_image_title_text');

/**
 *  This returns a link to the resource on the ResourceSpace server.
 */
function _media_resourcespace_remote_url($ref, $ext, $size,
                                        $include_key = TRUE) {
  $server_url = _media_resourcespace_get_server_url();
  $api_key = _media_resourcespace_get_api_key();

  $ref = check_plain($ref);
  $ext = check_plain($ext);
  $size = check_plain($size);
  $api_key = check_plain($api_key);
  $server_url = check_url($server_url);

  if ($size == MEDIA_RESOURCESPACE_SIZE_ORIGINAL) {
    $size_field = "size=";
  }
  elseif (is_numeric($size)) {
    $size_field = "size=&alternative=$size";
  }
  else {
    $size_field = "size=$size";
  }

  $url = $server_url
        . "/plugins/ref_urls/file.php?ref=$ref&ext=$ext&$size_field";

  if ($include_key) {
        $url .= "&key=$api_key";
  }

  return $url;
}

/**
** Get a URL to the ResourceSpace view.php page for the resource.
**/
function _media_resourcespace_remote_view_url($ref) {
  $ref = check_plain($ref);
  $server_url = _media_resourcespace_get_server_url();
  $url = $server_url . "/pages/view.php?ref=$ref";
  return $url;
}

/**
** Return a Drupal filesystem URI for the given ResourceSpace URI.
*/
function _media_resourcespace_to_local_uri($uri) {
  return str_replace('resourcespace://',
                'public://media-resourcespace/', $uri);
}

/*
** Resource types for searches.
*/
function _media_resourcespace_resource_types() {
  $resource_type_enum = array(
    'photo' => t('Photo'),
    'document' => t('Document'),
    'video' => t('Video'),
    'audio' => t('Audio'));

  return $resource_type_enum;
}

/**
 *  This is a wrapper for drupal_http_request that includes
 *  resourcespace's api key.
 *  @param string $path
 *  @param array $args
 *
 *  @return object
 *    A fully populated object decoded from the server JSON response.
 */
function _media_resourcespace_api_request($path, $args = array()) {
  // Display an error if we don't have an API key yet.
  if (!_media_resourcespace_error_check()) {
    return NULL;
  }

  $args['key'] = _media_resourcespace_get_api_key();

  $server_url = _media_resourcespace_get_server_url();
  $request_url = $server_url . $path;
  $request = drupal_http_request(url($request_url, array('query' => $args)));
  if (!isset($request->error)) {
    return json_decode($request->data);
  }
  else {
    drupal_set_message(
      t("Error accessing ResourceSpace (@code): @error",
        array('@code' => $request->code, '@error' => $request->error)),
      'error');

    return NULL;
  }
}

/**
 *  This will log an error if we don't have a key yet.
 *  In addition, if the user is an admin, we'll display an error.
 */
function _media_resourcespace_error_check() {
  static $checked;
  if (is_null($checked)) {
    $api_key = _media_resourcespace_get_api_key();
    if (empty($api_key)) {
      if (user_access('administer site configuration')) {
        $error = 'You do not yet have a ResourceSpace API key set. You will need to enter your key at the !settings or !usersettings before ResourceSpace images can be displayed.';
        $arguments = array('!settings' => l(t('ResourceSpace administration page'), 'admin/config/media/media_resourcespace'), '!usersettings' => l(t('user settings'), 'user'));
        drupal_set_message(t($error, $arguments), 'error');
      }
      else {
        $error = 'You do not yet have a ResourceSpace API key set. You will need to enter your key at !usersettings before ResourceSpace images can be displayed.';
        $arguments = array('!usersettings' => l(t('user settings', 'user')));
        drupal_set_message(t($error, $arguments), 'error');
      }
      watchdog('media_resourcespace', $error, $arguments);
      $checked = FALSE;
    }
    else {
    }

    $server_url = _media_resourcespace_get_server_url();
    if (empty($server_url)) {
      if (user_access('administer site configuration')) {
        $error = 'You do not yet have a ResourceSpace server set. You will need to enter your server URL at the !settings before ResourceSpace images can be displayed.';
        $arguments = array('!settings' => l(t('ResourceSpace administration page'), 'admin/config/media/media_resourcespace'));
        drupal_set_message(t($error, $arguments), 'error');
      }
      else {
        $error = 'You do not yet have a ResourceSpace server set. Contact your system administrator.';
        drupal_set_message(t($error), 'error');
      }
      watchdog('media_resourcespace', $error, $arguments);
      $checked = FALSE;
    }

    if (is_null($checked)) {
      $checked = TRUE;
    }
  }
  return $checked;
}

function _media_resourcespace_search_by_term($term,
            $resource_types = NULL, $date = NULL,
            $results_per_page = 12, $page = 1) {
  $restypes = "";

  if (isset($resource_types)) {
    foreach ($resource_types as $resource_type) {
      if (!empty($resource_type)) {
        if ($resource_type == 'photo') {
          $restypes="1,$restypes";
        }
        elseif ($resource_type == 'document') {
          $restypes="2,$restypes";
        }
        elseif ($resource_type == 'video') {
          $restypes="3,$restypes";
        }
        elseif ($resource_type == 'audio') {
          $restypes="4,$restypes";
        }
        else {
          throw new Exception("Unknown resource type: $resource_type");
        }
      }
    }
  }

  if (isset($date)) {
    if (!empty($date['year'])) {
      $term .= ',year:' . $date['year'];
    }

    if (!empty($date['month'])) {
      $month = str_pad($date['month'], 2, '0', STR_PAD_LEFT);
      $term .= ',month:' . $month;
    }
  }

  $url = "/plugins/api_search";

  $rs_params =
    array('search' => "$term", 'getsizes' => '1', 'getaltfiles' => '1',
        'metadata' => '1', 'prettyfieldnames' => '1',
        'restypes' => $restypes, 'results_per_page' => $results_per_page,
        'page' => $page, 'order_by' => 'relevance', 'sort' => 'DESC');

  $results = _media_resourcespace_api_request($url, $rs_params);

  if (isset($results)) {
    foreach ($results->resources as &$result) {
      _media_resourcespace_merge_alt_file_list($result);
    }
  }

  return $results;
}

function _media_resourcespace_search_by_ref($ref) {
  $results = _media_resourcespace_search_by_term("!list$ref");

  if ($results && count($results->resources) == 1) {
    return $results->resources[0];
  }

  return NULL;
}

/**
** Get information about the file referred to by a ResourceSpace URL.
**/
function _media_resourcespace_search_by_resourcespace_url($query) {
  # The URI is a redirect, such as a download.
  # Look at the target.
  if (isset($query['url'])) {
    $parts2 = parse_url($query['url']);
    $query = array();
    parse_str($parts2['query'], $query);
  }

  if (isset($query['ref'])) {
    $ref = $query['ref'];
    $ext = 'jpg';

    if (isset($query['ext'])) {
      $ext = $query['ext'];
    }

    $size = MEDIA_RESOURCESPACE_SIZE_ORIGINAL;
    if (isset($query['size']) && !empty($query['size'])) {
      $size = $query['size'];
    }

    // An alternative file has been requested.
    if (isset($query['alternative'])) {
      $size = $query['alternative'];
    }

    return _media_resourcespace_search_by_ref_size($ref, $ext, $size);
  }
}

/**
** Get information about the file referred to by a ResourceSpace ID and size.
**/
function _media_resourcespace_search_by_ref_size($ref, $ext, $size) {
  $search_result = _media_resourcespace_search_by_ref($ref);
  if (!isset($search_result)) {
    drupal_set_message(
          t("ResourceSpace resource @ref not found or accessible.",
          array('@ref' => $ref)), 'error');
    return NULL;
  }
  else {
    $size_result = _media_resourcespace_check_size($search_result,
                          $ext, $size);

    if (isset($size_result)) {
      return _media_resourcespace_build_search_result_info(
                  $search_result, $size_result);
    }
    else {
      return NULL;
    }
  }
}

/**
** Merge the list of alternative files into the other sizes - for this module
** there is no diference.
**/
function _media_resourcespace_merge_alt_file_list(&$result) {
  if (isset($result->altfiles)) {
    foreach ($result->altfiles as &$altfile) {
      $altfile->id = $altfile->alternative;
      $result->sizes[] = $altfile;
    }
  }
}

/**
** Extract file information from a ResourceSpace search result.
**/
function _media_resourcespace_build_search_result_info($search_result,
            $size_info) {
  if (empty($size_info->id)) {
    $size_info->id = MEDIA_RESOURCESPACE_SIZE_ORIGINAL;
    $size_info->name= t('Original');
  }

  $uri = _media_resourcespace_construct_url($search_result,
                  $size_info->extension, $size_info->id);

  $result = array();
  $result['uri'] = $uri;
  $result['size'] = $size_info->id;
  $result['size_name'] = $size_info->name;
  $result['ext'] = $size_info->extension;
  $result['ref'] = $search_result->ref;

  $result['title'] = '';
  $result['alt'] = '';

  if (isset($search_result->Title)) {
    $result['title'] = $search_result->Title;
    $result['alt'] = $search_result->Title;
  }

  if (isset($search_result->Caption)) {
    $result['alt'] = $search_result->Caption;
  }

  $result['width'] = '';
  if (isset($size_info->width) && $size_info->width != '?') {
    $result['width'] = $size_info->width;
  }

  $result['height'] = '';
  if (isset($size_info->height) && $size_info->height != '?') {
    $result['height'] = $size_info->height;
  }

  $result['filesizebytes'] = '';
  if (isset($size_info->filesizebytes)) {
    $result['filesizebytes'] = $size_info->filesizebytes;
  }

  $result['filesize'] = '';
  if (isset($size_info->filesize)) {
    $result['filesize'] = $size_info->filesize;
  }

  $result['orig_filename'] = '';
  if (isset($search_result->Original_filename)) {
    $result['orig_filename'] = $search_result->Original_filename;
  }

  if (isset($search_result->creation_date)) {
    $result['creation_date'] = $search_result->creation_date;
  }

  return $result;
}

/**
 * Add file information from the ResourceSpace search to a file object.
 */
function _media_resourcespace_set_file_data($file, $data) {
  if (isset($data['title'])) {
    $file->title = $data['title'];
  }

  if (isset($data['alt'])) {
    $file->alt = $data['alt'];
  }

  if (isset($data['filesize'])) {
    $file->filesize = $data['filesize'];
  }

  if (isset($data['filesizebytes'])) {
    $file->filesizebytes = $data['filesizebytes'];
  }

  if (isset($data['width'])) {
    $file->width= $data['width'];
  }

  if (isset($data['height'])) {
    $file->height= $data['height'];
  }

  return $file;
}

/**
** Perform validations specified in $validators, and validate file
** sizes and image resolutions using data from ResourceSpace.
*/
function _media_resourcespace_validate_file($file, $validators) {
  // Contains information about the field the media item will
  // be inserted into.
  $params = media_get_browser_params();
  $validators = _media_resourcespace_add_validators($validators, $params);

  if ($validators) {
    // Check for errors.
    // @see media_add_upload_validate calls file_save_upload().
    // this code is ripped from file_save_upload because we just want the
    // validation part.
    // Call the validation functions specified by this function's caller.

    // From Media: Youtube.
    // @TODO: Validate that if we have no $uri that this is a valid file to
    // save. For instance, we may only be interested in images, and it would
    // be helpful to let the user know they passed the HTML page containing
    // the image accidentally. That would also save us from saving the file
    // in the submit step.

    // This is kinda a hack of the same.

    // This should use the file_validate routines that the upload form users.
    // We need to fix the media_parse_to_file routine to allow for a
    // validation.

    $errors = file_validate($file, $validators);

    if (!empty($errors)) {
      $message = t('%uri could not be added.', array('%uri' => $file->uri));
      if (count($errors) > 1) {
        $message .= theme('item_list', array('items' => $errors));
      }
      else {
        $message .= ' ' . array_pop($errors);
      }
      form_set_error('url', $message);
      return FALSE;
    }
  }
}

/**
** Add file size and resolution validators to the given array.
**/
function  _media_resourcespace_add_validators($validators, $params) {
  if (isset($params['min_resolution'])) {
    $validators['_media_resourcespace_validate_image_resolution'] =
              array($params['min_resolution']);
  }

  if (isset($params['max_filesize'])) {
    $validators['_media_resourcespace_validate_file_size'] =
              array($params['max_filesize']);
  }

  return $validators;
}

/**
 * Check the size and resolution of the file against the field
 * it is being inserted into.
 */
function _media_resourcespace_validate_image_resolution($file,
                  $min_dimensions = NULL, $max_dimensions = NULL) {
  $errors = array();

  if (isset($min_dimensions)
      && property_exists($file, 'width') && property_exists($file, 'height')
      && isset($file->width) && isset($file->height)) {
    list($min_width, $min_height) = explode('x', $min_dimensions);
    if ($file->width < $min_width || $file->height < $min_height) {
      $errors[] = t('The image is too small; the minimum dimensions are %dimensions pixels.', array('%dimensions' => $min_dimensions));
    }
  }

  return $errors;
}

/**
 * Check the size and resolution of the file against the field
 * it is being inserted into.
 */
function _media_resourcespace_validate_file_size($file, $max_size = 0) {
  $errors = array();
  $size_bytes = parse_size($max_size);

  if (property_exists($file, 'filesizebytes') && isset($file->filesizebytes)
      && isset($max_size) && $size_bytes > 0
      && $file->filesizebytes > $size_bytes) {
    $errors[] = t('The file is too large; the maximum size is %size',
                    array('%size' => $max_size));
  }

  return $errors;
}

/**
** Construct the internal Media URL for a resource:
**  resourcespace://server/ref/1/ext/jpg/size/scr/file/filename.
*/
function _media_resourcespace_construct_url($search_result, $ext, $size) {
  # Don't do this - will cause problems if the ResourceSpace server moves.
  # Use "resourcespace" as a placeholder for allowing multiple ResourceSpace
  # servers later.
  # $server = _media_resourcespace_get_server_url();
  # $server = str_replace('http://', '', $server);
  # $server = str_replace('https://', '', $server);
  # $sanitised_server = str_replace('/', '__', $server);
  $sanitised_server = 'resourcespace';
  $path_parts = pathinfo($search_result->Original_filename);
  $filename = $path_parts['filename'];

  $ref = $search_result->ref;

  if (empty($filename)) {
    $filename = "RS$ref";
  }

  if (empty($size)) {
    $size = MEDIA_RESOURCESPACE_SIZE_ORIGINAL;
  }

  $uri = file_stream_wrapper_uri_normalize(
      "resourcespace://server/$sanitised_server/ref/${ref}/ext/${ext}/size/${size}/file/$filename" . '.' . $ext);
  return $uri;
}

function _media_resourcespace_check_size($search_result, $ext, $size) {
  foreach ($search_result->sizes as $found_size) {
    $found_ext = $found_size->extension;

    if ($found_ext == $ext &&
        ($found_size->id == $size
        || $size == MEDIA_RESOURCESPACE_SIZE_ORIGINAL
                && $found_size->id == ''
        )) {
      return $found_size;
    }
  }

  drupal_set_message(t(
    "ResourceSpace size @size and extension @ext for resource @ref not found",
      array('@ref' => $search_result->ref, '@ext' => $ext, '@size' => $size)),
      'error');

  return FALSE;
}

function _media_resourcespace_media_parse($url, $options = array()) {
  $result = _media_resourcespace_media_parse_with_metadata($url, $options);
  if (isset($result)) {
    return $result['uri'];
  }

  return NULL;
}

function _media_resourcespace_media_parse_with_metadata($url,
                $options = array()) {
  $parts = parse_url($url);
  $query = array();

  if (isset($parts['query'])) {
    parse_str($parts['query'], $query);

    // The following are single images.
    $patterns = array(
        "@/plugins/ref_urls/file\.php/?\?ref=([0-9]+)@i",
        "@/pages/download\.php/?\?ref=([0-9]+)@i",
        "@/pages/download_progress\.php/?\?ref=([0-9]+)@i",
        "@/pages/terms.php/?\?ref=([0-9]+)@i"
        );
    foreach ($patterns as $pattern) {
      preg_match($pattern, $url, $matches);
      if (isset($matches[1])) {
        return _media_resourcespace_search_by_resourcespace_url($query);
      }
    }
  }
}

/**
** Download the resource to the Drupal filesystem.
*/
function _media_resourcespace_download_resource($file, $remote_url) {
  // Copy the remote image locally, updating if already there.
  $dirname = drupal_dirname($file->uri);
  file_prepare_directory($dirname,
      FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

  if (@copy($remote_url, $file->uri)) {
    // If a user enters a duplicate URL, the object will be saved again.
    // Set the timestamp to the current time, so that the media item shows up
    // at the top of the media library, where they would expect to see it.
    $file->timestamp = REQUEST_TIME;
    return $file;
  }
  else {
    // Can't display $remote_url here because it contains the API key.
    $parts = drupal_parse_url($remote_url);
    $parts['query']['key'] = 'AUTH_KEY';
    $display_url = url($parts['path'], $parts);

    drupal_set_message(
        t('Copy from ResourceSpace using API key failed for @url. Check resource access and ResourceSpace configuration.',
        array('@url' => $display_url)), 'error');
    throw new Exception("Copy failed");
  }
}

/**
** Set the alt and title fields on an image file object.
*/
function _media_resourcespace_save_title_and_alt(&$file, $title, $alt) {
  $obj = entity_metadata_wrapper('file', $file);

  $alt_field = MEDIA_RESOURCESPACE_ALT_TEXT_FIELD;
  $title_field = MEDIA_RESOURCESPACE_TITLE_FIELD;

  // Work around a Wysiwyg bug - barfs on quotes in alt text.
  $alt = str_replace('"', "'", $alt);
  $title = str_replace('"', "'", $title);

  $file->title = $title;
  $file->alt = $alt;
  $file->timestamp = REQUEST_TIME;

  // These fields will only exist for images.
  if (property_exists($file, $title_field) && isset($title)) {
    $obj->$title_field->set($file->title);
  }

  if (property_exists($file, $alt_field) && isset($alt)) {
    $obj->$alt_field->set($file->alt);
  }

  $obj->save();
}

/**
** Get the URL for the ResourceSpace server.
*/
function _media_resourcespace_get_server_url() {
  $server_url = rtrim(variable_get('media_resourcespace_server_url', ''), '/');
  return $server_url;
}

function _media_resourcespace_get_api_key() {
  global $user;
  $obj = entity_metadata_wrapper('user', $user);

  $key = '';
  $field_name = 'field_resourcespace_auth_key';
  $field_value = $obj->$field_name->value();
  if (!empty($field_value)) {
    $key = $field_value;
  }
  else {
    $key = variable_get('media_resourcespace_api_key', '');
  }

  if (empty($key)) {
    $key = '';
  }

  return trim($key);
}

