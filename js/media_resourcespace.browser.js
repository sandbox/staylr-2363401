/**
 * @file
 * Handles the JS for the views file browser. Note that this does not currently
 * support multiple file selection
 */


(function ($) {

  Drupal.behaviors.mediaResourceSpaceBrowser = {
    attach: function (context, settings) {

      // Container for the files that get passed back to the browser
      var files = {};

      var submit = $("#media-resourcespace-add .form-submit[value='Submit']");

      // Click on a file size to continnue, not the submit button.
      submit.hide();

      // Disable the links on media items list
      $('.media-rs-resource-size a').click(function() {
        return false;
      });

      // Catch the click on a media item
      $('#media-resourcespace-add .media-item').bind('click', function () {
        // Remove all currently selected files
        $('.media-item').removeClass('selected');
        // Set the current item to active
        $(this).addClass('selected');
        // Add this FID to the array of selected files
        var uri = $(this).parent('a[data-uri]').attr('data-uri');
        var data = $(this).parent('a[data-resource-data]')
                          .attr('data-resource-data');

        // Get the file from the settings which was stored in
        // template_preprocess_media_views_view_media_browser()
        var file = Drupal.settings.media.files[uri];
        var files = new Array();
        files.push(file);
        Drupal.media.browser.selectMedia(files);
        $("input[name='submitted-resource']").val(uri);
        $("input[name='submitted-resource-data']").val(data);

        // TODO Only do this if not multi-select.
        //${"input[name='op']").val("Submit");
        submit.trigger("click");
      });

//      $('.')
    }
  }

}(jQuery));
