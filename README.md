# Media: ResourceSpace

Media: ResourceSpace integrates the [ResourceSpace](http://resourcespace.org)
Digital Asset Management system with the Media module to allow users to add
and manage ResourceSpace assets as they would any other piece of media.
Resources are downloaded to and served from Drupal - the ResourceSpace
server does not need to be open to the web.

## Requirements

- Media (7.x-2.x-dev 2014-Sep-29).
- File Entity (7.x-2.x-dev 2014-Sep-29).
- Views (7.x-3.8).
- Token (7.x-1.5).
- Entity (7.x-1.5).
- Chaos Tools (7.x-1.4).
- Date (7.x-2.8). Enable Date and Date API.

For Wysiwyg integration:

- Wysiwyg (7.x.2.2).
- CKEditor (max v3.6.6.2) (CKEditor itself, not the Drupal CKEditor module,
         integration is through Wysiwyg)
- Or TinyMCE 3.5.8

## Installation

Media: ResourceSpace can be installed using the standard Drupal installation
process (http://drupal.org/node/895232).

Enter your ResourceSpace server URL and API key into the Media: ResourceSpace
settings page (admin/configure/media/media\_resourcespace). This can also be
configured per-user on the user settings page.

For each image field that ResourceSpace assets could be used, make sure
"Media file selector" is selected as the Widget in the Content Type
configuration (admin/structure/types).

Make sure "Media Browser" is enabled in "Buttons and plugins" in the admin
page for each Wysiwyg profile, e.g.
http://<site>/#overlay=admin/config/content/wysiwyg/profile/filtered\_html/edit

## ResourceSpace Setup

Media: Resourcespace requires a patch to ResourceSpace to allow the ref\_urls plugin
to use an API key rather than a login session and api\_search to return the
available sizes for a file. Use the stdev-media-resourcespace branch in the Greens
BitBucket repository. TODO - ResourceSpace SVN/release version once pushed.

The following lines must appear in the ResourceSpace config.php.
Setting $data\_joins allows Drupal to use the original filename, title
and caption for the resource.

```
#!php
$enable_remote_apis = true;
$data_joins = array(18,51);
```

The api\_search and ref\_urls plugins must then be enabled. If ref\_urls
has previously been enabled, disable it and re-enable it to initialise the
new configuration form. Set to true the flag "Allow authentication of access
to resources using an API key rather than a login session." 

## Permissions

Media: ResourceSpace provides one permission:

- Administer Media: ResourceSpace
	Allows a user to configure ResourceSpace API keys.

## Usage

The Media "Select a file" dialog (file/add/web) now has a ResourceSpace tab
to select resources. Enter a search term (\* for all), then click on the
size from the list on the right for the resource to embed. Click on the thumbnail
to visit the ResourceSpace page for the resource.

The Media module allows internet media to be added using the "Web" tab of the
"Select a file" dialog (file/add/web).  With Media: ResourceSpace enabled
(ResourceSpace should appear in the list of supported providers), users can
add a ResourceSpace asset by entering its download URL. Cut and paste one of
the download links from the ResourceSpace view page for the resource into the
"Enter an image URL" field in Drupal.

## Limitations

Only a single ResourceSpace server can be configured.

## Related Modules 

A number of media providers exist for popular internet services.

- [Media: Flickr]((https://www.drupal.org/project/media_flickr)
- [Media: Vimeo]((https://www.drupal.org/project/media_vimeo)
- [Media: YouTube]((https://www.drupal.org/project/media_youtube)

For a full list of media providers, media players and other media
extensions, see
[Modules that Extend the Media Module](https://groups.drupal.org/node/168009).

## Credits

Media: ResourceSpace was developed using
[Media: Flickr](https://www.drupal.org/project/media_flickr) and
[Media: Flickr Local](https://www.drupal.org/sandbox/drothstein/2066499) as a base.

The search tab was adapted from 
[Media: YouTube](https://www.drupal.org/project/media_youtube).